from django.contrib import admin
from service_rest.models import Appointment, Technician, Status, AutomobileVO

@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_name = (
        "date_time",
        "reason",
        "customer",
        "vin",
        "status",
        "technician"
    )

@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    list_name = (
        "first_name",
        "last_name",
        "employee_id",
    )

@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    list_name = (
        "vin",
        "sold",
    )

@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    list_name = (
        "name",
    )


