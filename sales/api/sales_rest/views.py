import json
from .models import AutomobileVO, Salesperson, Customer, Sale
from .encoders import (
    SaleEncoder,
    SalespersonEncoder,
    CustomerEncoder,
)
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods


@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.all()
            return JsonResponse(
                {"salespeople": salespeople}, encoder=SalespersonEncoder
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salespeople url"},
                status=404,
            )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salespeople url to create salesperson"},
                status=404,
            )


@require_http_methods(["GET", "DELETE", "PUT"])
def show_salesperson(request, pk):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(pk=pk)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=404,
            )
    elif request.method == "DELETE":
        try:
            count, _ = Salesperson.objects.filter(pk=pk).delete()
            updatedSalespeople = Salesperson.objects.all()
            return JsonResponse(
                {"deleted": count > 0, "salespeople": updatedSalespeople},
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=404,
            )
    else:
        try:
            content = json.loads(request.body)
            Salesperson.objects.filter(pk=pk).update(**content)
            salesperson = Salesperson.objects.get(pk=pk)
            return JsonResponse(
                {"salesperson": salesperson},
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=404,
            )


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        try:
            customers = Customer.objects.all()
            return JsonResponse({"customers": customers}, encoder=CustomerEncoder)
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customers url"},
                status=404,
            )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customers url"},
                status=404,
            )


@require_http_methods(["GET", "DELETE", "PUT"])
def show_customer(request, pk):
    try:
        customer = Customer.objects.get(pk=pk)
    except Customer.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid customer id"},
            status=404,
        )
    if request.method == "GET":
        customer = Customer.objects.get(pk=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(pk=pk).delete()
        updatedCustomers = Customer.objects.all()
        return JsonResponse(
            {"deleted": count > 0, "customers": updatedCustomers},
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        Customer.objects.filter(pk=pk).update(**content)
        customer = Customer.objects.get(pk=pk)
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def list_sales(request, salesperson_id=None):
    if request.method == "GET":
        try:
            if salesperson_id == None is not None:
                sales = Sale.objects.filter(salesperson=salesperson_id)
            else:
                sales = Sale.objects.all()
                return JsonResponse({"sales": sales}, encoder=SaleEncoder)
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid sales url"},
                status=404,
            )
    else:
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile vin"},
                status=404,
            )
        try:
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=404,
            )
        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=404,
            )
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def show_sale(request, pk):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(pk=pk)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid sale id"},
                status=404,
            )

    elif request.method == "DELETE":
        try:
            count, _ = Sale.objects.filter(pk=pk).delete()
            updatedSale = Sale.objects.all()
            return JsonResponse(
                {"deleted": count > 0, "customers": updatedSale},
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid sale id"},
                status=404,
            )
    else:
        try:
            content = json.loads(request.body)
            Sale.objects.filter(pk=pk).update(**content)
            sale = Sale.objects.get(pk=pk)
            return JsonResponse(
                {"sale": sale},
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid sale id"},
                status=404,
            )
