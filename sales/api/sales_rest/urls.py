from .views import (
    list_customers,
    list_sales,
    list_salespeople,
    show_salesperson,
    show_sale,
    show_customer
)
from django.urls import path

urlpatterns = [
    path("salespeople/", list_salespeople, name="list_salespeople"),
    path("salespeople/<int:pk>/", show_salesperson, name="show_salesperson"),
    path("sales/", list_sales, name="list_sales"),
    path("sales/<int:pk>/", show_sale, name="show_sale"),
    path("customers/", list_customers, name="list_customers"),
    path("customers/<int:pk>/", show_customer, name="show_customer")
]
