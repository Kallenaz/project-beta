# LUX

Team:

* Lavanya - Sales
* Kallen (Kristian Allen) - Service

## Design
We will be using bootstrap for styling

## Service microservice

We will have a Technician, Appointment, Status, and  AutomobileVO model. The
technician model will hold information pertaining to each technician employee, the appointment model will hold information pertaining to a service appointment for a vehicle, and the status model will hold information about the status of an appointment. Lastly, the AutomobileVO will point to the Inventory microservice and poll the automobile vin and sold information for automobiles to use in our service microservice to determine whether or not a car coming in for service was purchased out of our inventory, aka, whether or not the customer is a vip.

## Sales microservice

Backend:
Sales microservice has four models including Sale, Customer, Salesperson, and AutomobilesVO(value object).

AutomobilesVO gets data from the main inventory microservice on automobile vin and sold information. Salesperson model has attributes of first name, last name, and employee id. Customer model has attributes of first name, last name, address, and phone number. Sale model has attributes of price, and three foreign keys including automobile, salesperson, and customer.

View functions will have specific requests for listing, creating, deleting, and updating specific sales, customer, and salesperson information.

We have included a poller function to obtain vin and sold data from the automobile database to be used in our sales microservice.

Frontend:
There are components for each sales, salesperson, salesperson history, and customer lists as well as their corresponding forms.

It is necessary for an automobile to be created in order to have a sale. Sale form will only show unsold vehicles and after sale has been made, will change unsold vehicle status to sold. There are filters in the salesperson history component that allow you to filter the sales history by certain employee attributes such as ID, first name or last name.

Delete buttons are also placed on lists, to allow for easy data manipulation, with automatic refresh after click.
