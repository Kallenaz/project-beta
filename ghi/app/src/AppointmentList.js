import React, { useState, useEffect } from "react";

function AppointmentList(props) {
    const [appointments, setAppointments] = useState([]);

    async function fetchAppointments() {
      const response = await fetch('http://localhost:8080/api/appointments/');
      if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments);
      } else {
        console.log(response)
      }
    }

    useEffect(() => {
      fetchAppointments();
    }, [])

    async function cancelAppointment(e,id) {
      e.preventDefault();

      const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {method: 'PUT'});

      if (response.ok) {
        fetchAppointments();
      }
    }

    async function finishAppointment(e,id) {
      e.preventDefault();

      const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {method: 'PUT'});

      if (response.ok) {
        fetchAppointments();
      }
    }

    function filterAppointments(appointment) {
      return appointment.status === "created";
    }

    return (
      <>
      <h1>Service Appointments:</h1>
      <table className="table table-hover">
        <thead>
          <tr>
            <th>Vin</th>
            <th>VIP?</th>
            <th>Customer</th>
            <th>Date/Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {appointments.filter(filterAppointments).map(appointment => {
            return (
              <tr key={appointment.vin}>
                <td>{ appointment.vin }</td>
                <td>{ appointment.vip ? "Yes" : "No" }</td>
                <td>{ appointment.customer }</td>
                <td>{ appointment.date_time }</td>
                <td>{ appointment.technician.first_name + " " + appointment.technician.last_name }</td>
                <td>{ appointment.reason }</td>
                <td>
                  <button type="button" onClick={(e) => cancelAppointment(e, appointment.id)} className="btn btn-warning">Canceled</button>
                </td>
                <td>
                  <button type="button" onClick={(e) => finishAppointment(e, appointment.id)} className="btn btn-success">Finished</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    );
  }

  export default AppointmentList;
