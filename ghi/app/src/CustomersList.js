import React, { useState, useEffect } from "react";

function CustomersList(props) {
    const [customers, setCustomers] = useState([]);
    async function fetchCustomers() {
        const response = await fetch('http://localhost:8090/api/customers/');
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setCustomers(data.customers);
        }
    }
    useEffect(() => {
        fetchCustomers();
    }, [])

    async function deleteCustomers(e, id) {
        e.preventDefault();
        const response = await fetch(`http://localhost:8090/api/customers/${id}`,
            { method: 'DELETE' });

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Address</th>
                    <th>Phone Number</th>
                </tr>
            </thead>
            <tbody>
                {customers.map(customer => {
                    return (
                        <tr key={customer.href}>
                            <td>{customer.first_name}</td>
                            <td>{customer.last_name}</td>
                            <td>{customer.address}</td>
                            <td>{customer.phone_number}</td>
                            <td>
                                <button type="button" onClick={(e) => deleteCustomers(e, customer.id)} className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody >
        </table >
    );
}
export default CustomersList;
