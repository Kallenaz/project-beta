import React, { useState, useEffect } from "react";

function AutomobileList(props) {
    const [autos, setAutos] = useState([]);
    async function fetchAutos() {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    }
    useEffect(() => {
        fetchAutos();
    }, [])

    async function deleteAutomobile(e, vin) {
        e.preventDefault();
        const response = await fetch(`http://localhost:8100/api/automobiles/${vin}`, { method: 'DELETE' });

        if (response.ok) {
            fetchAutos();
        }
    }

    return (
        <>
        <h1>Automobiles:</h1>
        <table className="table table-hover">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Sold</th>
                </tr>
            </thead>
            <tbody>
                {autos?.map(auto => {
                    return (
                        <tr key={auto.id}>
                            <td>{auto.vin}</td>
                            <td>{auto.color}</td>
                            <td>{auto.year}</td>
                            <td>{auto.model.name}</td>
                            <td>{auto.model.manufacturer.name}</td>
                            <td>{auto.sold ? 'Yes' : 'No'}</td>
                            <td>
                                <button type="button" onClick={(e) => deleteAutomobile(e, auto.vin)} className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody >
        </table >
        </>
    );
}
export default AutomobileList;
