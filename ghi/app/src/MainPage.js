import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import './MainPage.css';
import React, { useEffect } from 'react';

function MainPage() {
  useEffect(() => {
    const carouselInterval = setInterval(() => {
      const carousel = document.querySelector('#carouselExampleSlidesOnly');
      const activeItem = carousel.querySelector('.carousel-item.active');
      const nextItem = activeItem.nextElementSibling || carousel.firstElementChild;

      activeItem.classList.remove('active');
      nextItem.classList.add('active');
    }, 30);

    return () => clearInterval(carouselInterval);
  }, []);

  return (
    <div className="px-4 py-5 my-5 text-center bg-black">
      <h1 className="display-5 fw-bold text-white">Lux</h1>
      <div className="col-lg-6 mx-auto bg-black">
        <p className="lead mb-4 text-white bg-black">
          Luxury Perfected.
        </p>
      </div>
      <Carousel
        id="carouselExampleSlidesOnly"
        className="carousel slide bg-black"
        data-ride="carousel"
        interval={30} // Set to 0 to disable automatic sliding
        showStatus={false} // Hide the "1 of 1" indicator
      >
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img className="d-block w-100" src="https://www.hdcarwallpapers.com/walls/2018_maserati_ghibli_nerissimo_black_edition_4k-HD.jpg" alt="First slide" />
          </div>
          {/* <div className="carousel-item active">
            <img className="d-block w-100" src="https://rare-gallery.com/mocahbig/68145-Rolls-Royce-Car-Black-Car-Luxury-CarRolls-Royce.jpg" alt="Second slide" />
          </div> */}
          {/* <div className="carousel-item active"> */}
            {/* <img className="d-block w-100" src="https://di-uploads-pod37.dealerinspire.com/rollsroycemotorcarsdenversplash/uploads/2022/04/2022-ROLLS-ROYCE-Ghost-Black-Badge-jpg.jpg" alt="Third slide" />
          </div> */}
        </div>
      </Carousel>
    </div>
  );
}

export default MainPage;
// https://s1.1zoom.me/big0/726/382975-blackangel.jpg
