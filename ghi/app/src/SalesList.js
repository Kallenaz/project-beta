import React, { useState, useEffect } from "react";

function SalesList(props) {
    const [sales, setSales] = useState([]);
    async function fetchSales() {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setSales(data.sales);
        }
    }
    useEffect(() => {
        fetchSales();
    }, [])

    async function deleteSales(e, id) {
        e.preventDefault();
        const response = await fetch(`http://localhost:8090/api/sales/${id}`,
            { method: 'DELETE' });

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Salesperson Employee ID</th>
                    <th>Salesperson Name</th>
                    <th>Customer Name</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.map(sale => {
                    return (
                        <tr key={sale.href}>
                            <td>{sale.salesperson.employee_id}</td>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>${sale.price}</td>
                            <td>
                                <button type="button" onClick={(e) => deleteSales(e, sale.id)} className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody >
        </table >
    );
}
export default SalesList;
