import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CustomerForm from './CustomerForm';
import CustomersList from './CustomersList';
import SalespeopleList from './SalespeopleList';
import SalespersonForm from './SalespersonForm';
import SalespersonHistory from './SalespersonHistory';
import SaleForm from './SaleForm';
import SalesList from './SalesList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import ServiceHistoryList from './ServiceHistoryList';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import VehicleModelList from './VehicleModelList';
import VehicleModelForm from './VehicleModelForm';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="salespeople">
            <Route index element={<SalespeopleList salespeople={props.salespeople} />} />
            <Route path="new" element={<SalespersonForm />} />
            <Route path=":id" element={<SalespersonHistory />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomersList customers={props.customers} />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="sales">
            <Route index element={<SalesList sales={props.sales} />} />
            <Route path="new" element={<SaleForm />} />
          </Route>
          <Route path="technicians">
            <Route index element={<TechnicianList technicians={props.technicians} />} />
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentList appointments={props.appointments} />} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="history" element={<ServiceHistoryList appointments={props.appointments} />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList automobiles={props.automobiles} />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturerList manufacturers={props.manufacturers} />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={<VehicleModelList models={props.models} />} />
            <Route path="new" element={<VehicleModelForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
