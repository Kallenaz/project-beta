import React, { useState, useEffect } from "react";

function ManufacturerList(props) {
    const [manufacturers, setManufacturers] = useState([]);
    async function fetchManufacturers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }
    useEffect(() => {
        fetchManufacturers();
    }, [])

    async function deleteManufacturer(e, id) {
        e.preventDefault();
        const response = await fetch(`http://localhost:8100/api/manufacturers/${id}`,{ method: 'DELETE' });
        if (response.ok) {
            fetchManufacturers();
        }
    }

    return (
        <>
        <h1>Manufacturers:</h1>
        <table className="table table-hover">
            <thead>
                <tr>
                    <th>Manufacturer Name</th>
                </tr>
            </thead>
            <tbody>
                {manufacturers.map(manufacturer => {
                    return (
                        <tr key={manufacturer.href}>
                            <td>{manufacturer.name}</td>
                            <td>
                                <button type="button" onClick={(e) => deleteManufacturer(e, manufacturer.id)} className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody >
        </table >
        </>
    );
}
export default ManufacturerList;
