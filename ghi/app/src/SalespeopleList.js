import React, { useState, useEffect } from "react";

function SalespeopleList(props) {
    const [salespeople, setSalespeople] = useState([]);
    async function fetchSalespeople() {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setSalespeople(data.salespeople);
        }
    }
    useEffect(() => {
        fetchSalespeople();
    }, [])

    async function deleteSalesperson(e, id) {
        e.preventDefault();
        const response = await fetch(`http://localhost:8090/api/salespeople/${id}`,
            { method: 'DELETE' });

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
                {salespeople.map(salesperson => {
                    return (
                        <tr key={salesperson.href}>
                            <td>
                                {salesperson.employee_id}
                            </td>
                            <td>{salesperson.first_name}</td>
                            <td>{salesperson.last_name}</td>
                            <td>
                                <button type="button" onClick={(e) => deleteSalesperson(e, salesperson.id)} className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody >
        </table >
    );
}
export default SalespeopleList;
