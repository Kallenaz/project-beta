import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Lux</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item">
              <a className="nav-link dropdown-toggle" aria-current="page" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Salespeople
              </a>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDropdown">
                <li>
                  <NavLink className="nav-link dropdown-item text-white" to="/salespeople">Salespeople List</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link dropdown-item text-white" to="/salespeople/<int:id>">Salesperson History</NavLink>
                </li>
                <li><NavLink className="nav-link dropdown-item text-white" aria-current="page" to="/salespeople/new">New Salesperson</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item">
              <a className="nav-link dropdown-toggle" aria-current="page" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Customers
              </a>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDropdown">
                <li>
                  <NavLink className="nav-link dropdown-item text-white" to="/customers">Customers List</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link dropdown-item text-white" to="/customers/new">New Customer</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item">
              <a className="nav-link dropdown-toggle" aria-current="page" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </a>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDropdown">
                <li>
                  <NavLink className="nav-link dropdown-item text-white" to="/sales">Sales List</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link dropdown-item text-white" to="/sales/new">New Sale</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item">
              <a className="nav-link dropdown-toggle" aria-current="page" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Technicians
              </a>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDropdown">
                <li>
                  <NavLink className="nav-link dropdown-item text-white" to="/technicians">Technicians List</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link dropdown-item text-white" to="/technicians/new">New Technician</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item">
              <a className="nav-link dropdown-toggle" aria-current="page" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Appointments
              </a>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDropdown">
                <li>
                  <NavLink className="nav-link dropdown-item text-white" to="/appointments">Appointments List</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link dropdown-item text-white" to="/appointments/new">New Appointment</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link dropdown-item text-white" to="/appointments/history">Service History</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item">
              <a className="nav-link dropdown-toggle" aria-current="page" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Automobiles
              </a>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDropdown">
                <li>
                  <NavLink className="nav-link dropdown-item text-white" to="/automobiles">Automobiles List</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link dropdown-item text-white" to="/automobiles/new">New Automobile</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item">
              <a className="nav-link dropdown-toggle" aria-current="page" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Vehicle Models
              </a>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDropdown">
                <li>
                  <NavLink className="nav-link dropdown-item text-white" to="/models">Vehicle Models List</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link dropdown-item text-white" to="/models/new">New Vehicle Model</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item">
              <a className="nav-link dropdown-toggle" aria-current="page" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Manufacturers
              </a>
              <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDropdown">
                <li>
                  <NavLink className="nav-link dropdown-item text-white" to="/manufacturers">Manufacturers List</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link dropdown-item text-white" to="/manufacturers/new">New Manufacturer</NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div >
    </nav >
  )
}

export default Nav;
