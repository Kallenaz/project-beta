import React, { useState, useEffect } from "react";

function TechnicianList(props) {
    const [technicians, setTechnicians] = useState([]);
    async function fetchTechnicians() {
      const response = await fetch('http://localhost:8080/api/technicians/');
      if (response.ok) {
        const data = await response.json();
        setTechnicians(data.technicians);
      }
    }

    useEffect(() => {
      fetchTechnicians();
    }, [])

    async function deleteTechnician(e,id) {
      e.preventDefault();

      const response = await fetch(`http://localhost:8080/api/technicians/${id}`, {method: 'DELETE'});

      if (response.ok) {
        const data = await response.json();
        setTechnicians(data.technicians);
      }
    }

    return (
      <>
      <h1>Technicians:</h1>
      <table className="table table-hover">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map(technician => {
            return (
              <tr key={technician.id}>
                <td>{ technician.employee_id }</td>
                <td>{ technician.first_name }</td>
                <td>{ technician.last_name }</td>
                <td>
                  <button type="button" onClick={(e) => deleteTechnician(e, technician.id)} className="btn btn-danger">Remove</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    );
  }

  export default TechnicianList;
