import React, { useState, useEffect } from "react";

function ServiceHistoryList(props) {

    const [appointments, setAppointments] = useState([]);
    const [filterVIN, setFilterVIN] = useState("")

    async function fetchAppointments() {
      const response = await fetch('http://localhost:8080/api/appointments/');
      if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments);
      } else {
        console.log(response)
      }
    }

    useEffect(() => {
      fetchAppointments();
    }, [])

  const handleFilterVINChange =  (e) => {
      setFilterVIN(e.target.value);
  }

  const filterApptByVIN = function (appointment) {
    return appointment.vin === filterVIN;
  }

  const handleSearch = function () {
    if (filterVIN === "") {
      fetchAppointments();
    } else {
      const filteredAppointments = appointments.filter(filterApptByVIN);
      setAppointments(filteredAppointments);
    }
  }



    return (
      <>
      <h1>Service History:</h1>
      <input onChange={handleFilterVINChange} placeholder='Search by VIN'/>
      <button className="btn btn-primary" onClick={handleSearch} type="submit">Search</button>
      <button className="btn btn-dark" onClick={fetchAppointments} type="submit">Reset</button>

      <table className="table table-hover">
        <thead>
          <tr>
            <th>Vin</th>
            <th>VIP?</th>
            <th>Customer</th>
            <th>Date/Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map(appointment => {
            return (
              <tr key={appointment.vin}>
                <td>{ appointment.vin }</td>
                <td>{ appointment.vip ? "Yes" : "No" }</td>
                <td>{ appointment.customer }</td>
                <td>{ appointment.date_time }</td>
                <td>{ appointment.technician.first_name + " " + appointment.technician.last_name }</td>
                <td>{ appointment.reason }</td>
                <td>{ appointment.status }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    );
  }

  export default ServiceHistoryList;
