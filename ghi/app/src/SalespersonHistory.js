import React, { useState, useEffect } from "react";

function SalespersonHistory(props) {
    const [salespeople, setSalespeople] = useState([]);
    const [sales, setSales] = useState([]);
    const [filterOption, setFilterOption] = useState('');
    const [filteredData, setFilteredData] = useState([]);

    const fetchDataSalesPeople = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }
    useEffect(() => {
        fetchDataSalesPeople();
    }, []);
    const optionSelected = (e) => {
        const value = e.target?.value || '';
        setFilterOption(value);
        filterOptionSelected(value);
    };

    async function fetchSales() {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }
    useEffect(() => {
        fetchSales();
    }, []);


    const salesDataTable = (sales) => {
        return sales.map((sale, i) => (
            <tr key={i}>
                <td>{sale.salesperson.employee_id}</td>
                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
            </tr>
        ))
    }
    const filterOptionsDropDown = (filterOption) => {
        if (filterOption) {
            let options = salespeople.filter((salesperson) => salesperson[filterOption]);
            return options.map((option, i) => (
                <option key={i} value={option[filterOption]}>
                    {option[filterOption]}
                </option>
            ));
        }
        return null;
    };
    const filterOptionSelected = (e) => {
        let value = e.target.value;
        if (value) {
            let filteredData = sales.filter(
                (sale) =>
                    sale.salesperson.employee_id === value ||
                    sale.salesperson.first_name === value ||
                    sale.salesperson.last_name === value
            );
            setFilteredData(filteredData);
        } else {
            setFilteredData(sales);
        }
    };

    return (
        <div>
            <div>
                <label> Filter By </label>
                <select onChange={optionSelected}>
                    <option value="">  </option>
                    <option value="employee_id"> Salesperson Employee ID </option>
                    <option value="first_name"> Salesperson First Name </option>
                    <option value="last_name"> Salesperson Last Name </option>
                </select>
                <select onChange={filterOptionSelected}>
                    <option value="">  </option>
                    {filterOptionsDropDown(filterOption)}
                </select>
            </div>
            <table border="1">
                <thead>
                    <tr>
                        <th> Salesperson Employee ID </th>
                        <th> Salesperson Name </th>
                        <th> Customer Name </th>
                        <th> VIN </th>
                        <th> Price </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        filteredData.length ? salesDataTable(filteredData) : salesDataTable(sales)
                    }
                </tbody>
            </table>
        </div>
    );
}


export default SalespersonHistory;
